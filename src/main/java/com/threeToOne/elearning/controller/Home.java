package com.threeToOne.elearning.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframewor
k.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.threeToOne.elearning.form.RegisterForm;
import com.threeToOne.elearning.helper.DefaultPasswordHasher;
import com.threeToOne.elearning.model.Course;
import com.threeToOne.elearning.model.Student;
import com.threeToOne.elearning.repository.CourseRepository;
import com.threeToOne.elearning.repository.StudentRepository;
//
@Controller("/")
public class Home {

	@Autowired
	StudentRepository studentRepository;
	@Autowired
	CourseRepository courseRepository;
	
	
	@PostMapping("/createCourse")	
	public @ResponseBody Map<String, Object> createCourse(@RequestBody Course course){
		
		Map<String,Object> map=new HashMap<>();
		try {
			courseRepository.save(course);
			map.put("status", 200);
		}catch(Exception e) {
			map.put("status", 404);
			return map;
		}
		return map;
	}
@PostMapping("/register")	
public @ResponseBody Map<String, Object> register(@Valid @RequestBody RegisterForm registerForm){
	Map<String,Object> map=new HashMap<>();
	try {
		Student student=new Student();
		student.setName(registerForm.getName());
		student.setUserName(registerForm.getUserName());
		student.setEmail(registerForm.getEmail());
		student.setDOB(registerForm.getDOB());
		student.setGender(registerForm.getGender());
		//student.setPassword(registerForm.getPassword());
		student.setPassword(DefaultPasswordHasher.getInstance().hashPassword(registerForm.getPassword()));
		studentRepository.save(student);
		
		Course course=courseRepository.findByName(registerForm.getCourseName());
		System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+course.getName());
		course.setStudent(student);
		courseRepository.save(course);
		map.put("staus", 200);
	
		map.put("studentId", student.getId());
	}catch(Exception e) {
		e.printStackTrace();
		map.put("status", 404);
		return map;
	}
	return map;
}

@PostMapping("/login")	
public @ResponseBody Map<String, Object> login(@RequestBody Student student){
	Map<String,Object> map=new HashMap<>();
	try {
		   Student student1=studentRepository.findByUserName(student.getUserName());
		   //if(student.getPassword().equals(student1.getPassword())) {
		   if (DefaultPasswordHasher.getInstance().isPasswordValid(student.getPassword(), student1.getPassword())) {
			   map.put("status", 200);
			   map.put("studentId", student1.getId());
		   }else {
			   
				map.put("status", 404);
		   }
		
	}catch(Exception e) {
		e.printStackTrace();
		map.put("status", 404);
		return map;
	}
	return map;
}

@PostMapping("/unRegister")	
public @ResponseBody Map<String, Object> unRegister(@Valid @RequestBody RegisterForm registerForm){
	Map<String,Object> map=new HashMap<>();
	try {
		
			Course course=courseRepository.findByName(registerForm.getCourseName());
			System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+course.getName());
			course.setStudent(null);
			courseRepository.save(course);
			map.put("staus", 200);
	}catch(Exception e) {
		e.printStackTrace();
		map.put("status", 404);
		return map;
	}
	return map;
}

@PostMapping("/StudentCourses")	
public @ResponseBody Map<String, Object> getStudentCourses(@RequestBody Student student){
	Map<String,Object> map=new HashMap<>();
	try {
	        Student student1=studentRepository.getOne(student.getId());
	        map.put("status", 200);
		    map.put("courses", student1.getCourse());
	}catch(Exception e) {
		e.printStackTrace();
		map.put("status", 404);
		return map;
	}
	return map;
	}

@GetMapping("/availableCourses")	
public @ResponseBody Map<String, Object> availableCourse(){
	Map<String,Object> map=new HashMap<>();
	try {
	        
	        map.put("status", 200);
		    map.put("courses", courseRepository.findAll());
	}catch(Exception e) {
		e.printStackTrace();
		map.put("status", 404);
		return map;
	}
	return map;
	}
	//update course by id
@PostMapping("/updateCourse")	
public @ResponseBody Map<String, Object> updateCourse(@Valid @RequestBody Course course){
	Map<String,Object> map=new HashMap<>();
	try {
		
			Course course1=courseRepository.findByName(course.getName());
			if(course.getName()!=null) {
				course1.setName(course.getName());
			}
			if(course.getDescription()!=null) {
				course1.setName(course.getDescription());
			}
			if(course.getPublish()!=null) {
				course1.setPublish(course.getPublish());
			}
			if(course.getInstructor()!=null) {
				course1.setInstructor(course.getInstructor());
			}
			if(course.getTotalHours()!=0) {
				course1.setTotalHours(course.getTotalHours());
			}
			if(course.getUpdated()!=null) {
				course1.setUpdated(course.getUpdated());
			}
			
			System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+course.getName());
			//course1.setStudent(null);
			courseRepository.save(course1);
			map.put("staus", 200);
			
	}catch(Exception e) {
		e.printStackTrace();
		map.put("status", 404);
		return map;
	}
	return map;
}
//delete course by id
@PostMapping("/deleteCourse")	
public @ResponseBody Map<String, Object> deleteCourse(@RequestBody Course course){
	Map<String,Object> map=new HashMap<>();
	try {
		Course course1=courseRepository.findByName(course.getName());
		   courseRepository.deleteById(course1.getId());
		   map.put("staus", 200);
	}catch(Exception e) {
		e.printStackTrace();
		map.put("status", 404);
		return map;
	}
	return map;
}
//update student by id
@PostMapping("/updateStudent")	
public @ResponseBody Map<String, Object> updateStudent(@Valid @RequestBody Student student){
	Map<String,Object> map=new HashMap<>();
	try {
		
			Student student1=studentRepository.getOne(student.getId());
			if(student.getName()!=null) {
				student1.setName(student.getName());
			}
			if(student.getDOB()!=null) {
				student1.setDOB(student.getDOB());
			}
			if(student.getEmail()!=null) {
				student1.setEmail(student.getEmail());
			}
			if(student.getGender()!=null) {
				student1.setGender(student.getGender());
			}
			if(student.getPassword()!=null) {
				student1.setPassword(student.getPassword());
			}
			if(student.getUserName()!=null) {
				student1.setUserName(student.getUserName());
			}
			
			System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+student.getName());
			//course1.setStudent(null);
			studentRepository.save(student1);
	}catch(Exception e) {
		e.printStackTrace();
		map.put("status", 404);
		return map;
	}
	return map;
}
//delete by student id
@PostMapping("/deleteStudent")	
public @ResponseBody Map<String, Object> deleteStudent(@RequestBody Student student){
	Map<String,Object> map=new HashMap<>();
	try {
		//Course student1=courseRepository.findByName(student.getName());
		   studentRepository.deleteById(student.getId());
		   map.put("staus", 200);
	}catch(Exception e) {
		e.printStackTrace();
		map.put("status", 404);
		return map;
	}
	return map;
}
	
}
