package com.threeToOne.elearning.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.threeToOne.elearning.model.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

	@Query("select c from Course c where c.name =:name")
	  Course findByName(@org.springframework.data.repository.query.Param(value = "name") String name);
}
