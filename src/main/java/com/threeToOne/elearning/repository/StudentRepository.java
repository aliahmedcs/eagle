package com.threeToOne.elearning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.threeToOne.elearning.model.Course;
import com.threeToOne.elearning.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

	@Query("select s from Student s where s.userName =:userName")
	  Student findByUserName(@org.springframework.data.repository.query.Param(value = "userName") String userName);
}
